Feature: Add Quick Task

  @Tag
  Scenario: Test Scenario

    As as user, I wish to be able to test my application, etc.
    Given I have the app open
    And I am on the main screen
    When I put in a quick task of "This is my test task"
    Then I should see "This is my test task" in the task list