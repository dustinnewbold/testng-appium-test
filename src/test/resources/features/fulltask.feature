Feature: Full task
  Scenario: Things

    Given I am on the main screen
    When I tap the floating action button
    And I put in a task of "This is another test task"
    And I tap on task complete button
    Then I should see "This is another test task" in the task list