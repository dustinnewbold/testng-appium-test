package com.dustinnewbold.pages.objects;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class TaskPageObject {
    @AndroidFindBy(id = "com.splendapps.splendo:id/action_save_task")
    public MobileElement saveTask;

    @AndroidFindBy(id = "com.splendapps.splendo:id/edtTaskName")
    public MobileElement taskName;

    @AndroidFindBy(id = "com.splendapps.splendo:id/edtDueD")
    public MobileElement dueDate;
}
