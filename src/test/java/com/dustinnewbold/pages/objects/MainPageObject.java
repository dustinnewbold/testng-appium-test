package com.dustinnewbold.pages.objects;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import java.util.List;

public class MainPageObject {
    @AndroidFindBy(id = "com.splendapps.splendo:id/etQuickTask")
    public MobileElement quickEntry;

    @AndroidFindBy(id = "com.splendapps.splendo:id/ivAddQuickTask")
    public MobileElement addQuickTask;

    @AndroidFindBy(id = "com.splendapps.splendo:id/task_list_item")
    public List<MobileElement> taskList;

    @AndroidFindBy(id = "com.splendapps.splendo:id/ivFAB_AddTask")
    public MobileElement floatingActionButton;

    @AndroidFindBy(id = "com.splendapps.splendo:id/action_search")
    public MobileElement searchButton;

    @AndroidFindBy(id = "com.splendapps.splendo:id/tvEmpty")
    public MobileElement notFoundText;

    @AndroidFindBy(id = "com.splendapps.splendo:id/toolbar")
    public MobileElement searchBar;


    public MobileElement searchBackButton;

    public void init() {
//        searchBackButton = searchBar.findElementByClassName("android.widget.ImageButton");
    }
}
