package com.dustinnewbold.pages;

import com.dustinnewbold.pages.objects.TaskPageObject;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class TaskPage {
    private TaskPageObject page;

    public TaskPage(AppiumDriver driver) {
        this.page = new TaskPageObject();
        PageFactory.initElements(new AppiumFieldDecorator(driver), this.page);
    }

    public void addTaskName(String name) {
        page.taskName.click();
        page.taskName.sendKeys(name);
    }

    public void tapSaveTask() {
        page.saveTask.click();
    }
}
