package com.dustinnewbold.pages;

import com.dustinnewbold.pages.objects.MainPageObject;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class MainPage {
    private MainPageObject page;

    public MainPage(AppiumDriver driver) {
        page = new MainPageObject();
        PageFactory.initElements(new AppiumFieldDecorator(driver), page);
        page.init();
    }



    public void addQuickTask(String task) {
        page.quickEntry.click();
        page.quickEntry.sendKeys(task);
        page.addQuickTask.click();
    }

    public boolean taskExists(String taskName) {
        for ( MobileElement task : page.taskList ) {
            MobileElement name = task.findElementById("com.splendapps.splendo:id/task_name");
            if ( name.getText().equals(taskName) ) return true;
        }

        return false;
    }

    public void tapSearchButton() {
        page.searchButton.click();
    }

    public boolean isNotFoundShown() {
        return page.notFoundText.isDisplayed();
    }

    public String getNotFoundText() {
        if ( ! isNotFoundShown() ) return "";
        return page.notFoundText.getText();
    }


    /**
     * Buttons
     */

    public void tapAddTask() {
        page.floatingActionButton.click();
    }

    public void tapSearchBackButton() {
//        page.searchBackButton.click();
    }
}
