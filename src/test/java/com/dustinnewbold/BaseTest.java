package com.dustinnewbold;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.net.URL;

public class BaseTest {
    protected AppiumDriver driver;

    @BeforeClass
    @Parameters({ "deviceName" })
    public void setUpBaseTest(String deviceName) throws Throwable {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.UDID, deviceName);
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "android");
        capabilities.setCapability(MobileCapabilityType.APP_PACKAGE, "com.splendapps.splendo");
        capabilities.setCapability(MobileCapabilityType.APP_ACTIVITY, ".MainActivity");
        capabilities.setCapability(MobileCapabilityType.APP, "/data/toybox/appium/apps/todolist.apk");

        driver = new AndroidDriver(new URL("http://127.0.0.1:4444/wd/hub"), capabilities);
    }

    @AfterClass
    public void tearDownBaseTest() {
        driver.quit();
    }
}
