package com.dustinnewbold.tests;

import com.dustinnewbold.BaseTest;
import com.dustinnewbold.pages.MainPage;
import com.dustinnewbold.pages.TaskPage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TaskTest extends BaseTest {
    MainPage mainPage;
    TaskPage taskPage;

    @BeforeClass
    public void setUp() {
        mainPage = new MainPage(driver);
        taskPage = new TaskPage(driver);
    }

    @Test(groups = "taskAdds")
    public void testQuickTaskAdd() {
        String task = "This is a quick added task";
        mainPage.addQuickTask(task);
        Assert.assertEquals(mainPage.taskExists(task), true);
    }

    @Test(groups = "taskAdds")
    public void testSimpleTaskAdd() {
        String task = "This is a simple task";
        mainPage.tapAddTask();
        taskPage.addTaskName(task);
        taskPage.tapSaveTask();
        Assert.assertEquals(mainPage.taskExists(task), true);
    }

    @Test(dependsOnGroups = "taskAdds")
    public void testFailedSearchTask() {
        String searchTask = "SHOULDNOTBEFOUND";
        mainPage.tapSearchButton();
        driver.getKeyboard().sendKeys(searchTask);
        Assert.assertEquals(mainPage.isNotFoundShown(), true);

        // There's an added space for some reason
        Assert.assertEquals(mainPage.getNotFoundText(), " " + searchTask + " not found");

        mainPage.tapSearchBackButton();
    }
}
